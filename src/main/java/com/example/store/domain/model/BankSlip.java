package com.example.store.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@Data
public class BankSlip implements PaymentInstrument{

    private String number;
    private String ownerName;

    @Override
    public void validate() throws Exception {
        if (!this.number.isEmpty() && !this.ownerName.isEmpty()) {
            return;
        }

        throw new Exception("It was not possible to validate the payment slip data.");
    }

    @Override
    public void processPayment() throws Exception {
        if (this.ownerName.endsWith("a") || this.number.endsWith("0")){
            throw new Exception("It was not possible to process the bank slip.");
        }

        return;
    }
}
