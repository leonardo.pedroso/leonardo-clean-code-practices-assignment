package com.example.store.domain.model;

public interface PaymentInstrument {
    public void validate() throws Exception;
    public void processPayment() throws Exception;
}
