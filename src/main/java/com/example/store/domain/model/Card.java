package com.example.store.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@Data
public class Card implements PaymentInstrument{

    private String number;
    private String ownerName;
    private String cvv;

    @Override
    public void validate() throws Exception {
        if (!this.number.isEmpty() && !this.ownerName.isEmpty() && !this.cvv.isEmpty()) {
            return;
        }
        throw new Exception("It was not possible to validate the card details.");
    }

    @Override
    public void processPayment() throws Exception {
        if (this.ownerName.endsWith("b") || this.number.endsWith("1")){
            throw new Exception("Unable to process payment via card.");
        }
        return;
    }
}
