package com.example.store.domain.exception;

public class ProcessingException extends RuntimeException {
    public ProcessingException(){
        super("ERROR_PROCESSING_PAYMENT_INSTRUMENT");
    }
}
