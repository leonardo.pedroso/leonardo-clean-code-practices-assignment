package com.example.store.app.controller;

import com.example.store.app.service.PaymentService;
import com.example.store.domain.exception.ProcessingException;
import com.example.store.app.dto.BankSlipRequest;
import com.example.store.app.dto.CardRequest;
import com.example.store.app.dto.PaymentResponse;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class PaymentController {

    private final PaymentService service;

    @PostMapping("/pay-card")
    public PaymentResponse cardPayment(@RequestBody CardRequest cardRequest) throws ProcessingException {
        return service.processCardPayment(cardRequest);
    }

    @PostMapping("/pay-slip")
    public PaymentResponse slipPayment(@RequestBody BankSlipRequest bankSlipRequest) throws ProcessingException {
        return service.processBankSlipPayment(bankSlipRequest);
    }


}
