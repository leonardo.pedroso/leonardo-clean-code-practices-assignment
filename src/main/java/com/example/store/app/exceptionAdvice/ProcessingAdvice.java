package com.example.store.app.exceptionAdvice;

import com.example.store.domain.exception.ProcessingException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ProcessingAdvice {
    @ResponseBody
    @ExceptionHandler(ProcessingException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public String errorProcessingHandler(ProcessingException exception) {
        return exception.getMessage();
    }
}
