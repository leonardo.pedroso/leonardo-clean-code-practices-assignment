package com.example.store.app.service;

import com.example.store.app.dto.BankSlipRequest;
import com.example.store.app.dto.CardRequest;
import com.example.store.app.dto.PaymentResponse;
import com.example.store.domain.model.BankSlip;
import com.example.store.domain.model.Card;
import com.example.store.domain.exception.ProcessingException;
import com.example.store.domain.model.PaymentInstrument;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class PaymentService {

    private final static ModelMapper mapper = new ModelMapper();

    public PaymentResponse processBankSlipPayment(BankSlipRequest bankSlipRequest) {
        val instrument = mapper.map(bankSlipRequest, BankSlip.class);
        return processPaymentInterface(instrument);
    }

    public PaymentResponse processCardPayment(CardRequest cardRequest) {
        val instrument = mapper.map(cardRequest, Card.class);
        return processPaymentInterface(instrument);
    }

    private PaymentResponse processPaymentInterface(PaymentInstrument instrument) {
        val processedInstrument = processPayment(instrument);
        return mapper.map(processedInstrument, PaymentResponse.class);
    }

    public PaymentInstrument processPayment(PaymentInstrument instrument) {
        try{
            instrument.validate();
            instrument.processPayment();
            log.info("The payment instrument has been successfully validated and processed.");
            return instrument;
        } catch (Exception e){
            log.error("It was not possible to validate the payment instrument. {}", e.toString());
            throw new ProcessingException();
        }
    }
}
