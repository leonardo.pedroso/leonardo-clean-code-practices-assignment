package com.example.store.app.dto;

import lombok.Data;

@Data
public class BankSlipRequest {
    private String number;
    private String ownerName;
}
