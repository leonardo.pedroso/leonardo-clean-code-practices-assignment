package com.example.store.app.dto;

import lombok.Data;

@Data
public class CardRequest {
    private String number;
    private String ownerName;
    private String cvv;
}
