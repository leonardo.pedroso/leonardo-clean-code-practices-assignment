package com.example.store.app.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class PaymentResponse {
    private String number;
    private String ownerName;
    private LocalDateTime createdAt;

    public PaymentResponse(String number, String ownerName) {
        this.number = number;
        this.ownerName = ownerName;
        this.createdAt = LocalDateTime.now();
    }
}
